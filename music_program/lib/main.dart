import 'package:flutter/material.dart';

var selectedKeys = [];
var letters = ['A', 'Ab', 'B', 'Bb', 'C', 'Db', 'D', 'Eb', 'E', 'F', 'F#', 'G'];

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
            seedColor: Colors.deepPurple, background: Colors.blue),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: [
      Container(
          height: 450,
          width: 600,
          padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
          child: InteractiveViewer(
              boundaryMargin: const EdgeInsets.all(20.0),
              minScale: .1,
              maxScale: 1.6,
              child: CustomPaint(painter: ShapeDrawer(), child: Container()))),
      Container(
          margin: EdgeInsets.fromLTRB(325, 50, 50, 50), child: pianoKeys())
    ]));
  }

  Row pianoKeys() {
    List<Widget> rows = [];
    for (int i = 0; i < letters.length; i += 1) {
      rows.add(SizedBox(
          width: 80,
          height: 150,
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              ),
              onPressed: () {
                if (selectedKeys.contains(letters[i])) {
                  selectedKeys.remove(letters[i]);
                } else {
                  selectedKeys.add(letters[i]);
                }
                print(selectedKeys);
              },
              child: Text(letters[i]))));
    }
    return Row(children: rows);
  }
}

class ShapeDrawer extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawColor(Colors.white, BlendMode.clear);
    var paint = Paint();
    paint.color = Colors.green[800]!;
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = 5;
    var path = Path();
    path.lineTo(size.width, size.height);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
